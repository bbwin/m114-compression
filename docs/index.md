# Einleitung

!!! abstract "Lernziel"

    In diesem Dokument lernen Sie, eine containerisierte Applikation in Amazon Elastic Container Registry hochzuladen und in Amazon Elastic Container Service bereitzustellen.

| Produkte                                                     |
| ------------------------------------------------------------ |
| <img src="assets/5969282.png" alt="img" style="width:32px;" /> WSL2 Ubuntu Terminal |
| <img src="assets/vscode.png" alt="vscode" style="width:32px;" /> Visual Studio Code mit [Remote Development extension pack](https://aka.ms/vscode-remote/download/extension) |
